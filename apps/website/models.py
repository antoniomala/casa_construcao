# -*- coding: utf-8 -*-
from django.db import models
from django import forms
from django.conf import settings
from django.db.models.signals import post_save

class Cabecalho(models.Model):
	nome_empresa = models.CharField(u'Nome da empresa', max_length=25)
	frase = models.CharField(u'Frase de impacto', max_length=120)
	lema = models.CharField(u'Lema da empresa', max_length=250)

class InfomacoesBasicas(models.Model):
	bem_vindo = models.CharField(u'mensagem de boas vindas', max_length=250)
	breve_descricao = models.TextField(u'brave descrição')

class PostLateral(models.Model):
	imagem = models.FileField(u'imagem do post', upload_to=settings.MEDIA_ROOT, blank=True, null=True)
	titulo = models.CharField(u'Titulo do post', max_length=50)
	descricao = models.TextField(u'descrição do post')

class PostVertical(models.Model):
	imagem = models.FileField(u'imagem do post', upload_to=settings.MEDIA_ROOT, blank=True, null=True)
	titulo = models.CharField(u'Titulo do post', max_length=50)
	descricao = models.TextField(u'descrição do post')	
	segundo_paragrafo = models.TextField(u'segundo paragrafo do post(opcional)', blank=True, null=True)	