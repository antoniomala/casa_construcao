# -*- coding: utf-8 -*-
from django.shortcuts import render
from .models import *


def home_page(request):
	return render(request, 'home_page.html')